var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Nivel1 = require('../models/nivel1');

// ==========================================
// Obtener todos las niveles1
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Nivel1.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .exec(
            (err, niveles1) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando nivel1',
                        errors: err
                    });
                }

                Nivel1.count({}, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        niveles1: niveles1,
                        total: conteo
                    });
                })

            });
});

// ==========================================
//  Obtener Nivel1 por ID
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Nivel1.findById(id)
        .populate('usuario', 'nombre img email')
        .exec((err, nivel1) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar nivel1',
                    errors: err
                });
            }

            if (!nivel1) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El nivel1 con el id ' + id + 'no existe',
                    errors: { message: 'No existe un nivel1 con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                nivel1: nivel1
            });
        })
})





// ==========================================
// Actualizar Nivel1
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Nivel1.findById(id, (err, nivel1) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar nivel1',
                errors: err
            });
        }

        if (!nivel1) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El nivel1 con el id ' + id + ' no existe',
                errors: { message: 'No existe un nivel1 con ese ID' }
            });
        }


        nivel1.nombre = body.nombre;
        nivel1.usuario = req.usuario._id;

        nivel1.save((err, nivel1Guardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar nivel1',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                nivel1: nivel1Guardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo nivel1
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var nivel1 = new Nivel1({
        nombre: body.nombre,
        usuario: req.usuario._id
    });

    nivel1.save((err, nivel1Guardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear nivel1',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            nivel1: nivel1Guardado
        });


    });

});


// ============================================
//   Borrar un nivel1 por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Nivel1.findByIdAndRemove(id, (err, nivel1Borrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar nivel1',
                errors: err
            });
        }

        if (!nivel1Borrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nivel1 con ese id',
                errors: { message: 'No existe un nivel1 con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            nivel1: nivel1Borrado
        });

    });

});


module.exports = app;