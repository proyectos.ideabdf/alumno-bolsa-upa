var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Usuario = require('../models/usuario');
var InscritoNota = require('../models/inscritoNota');



// ==============================================
// Crear un nuevo registro de nota para inscrito
// ==============================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var inscritoNota = new InscritoNota({
        notaTeorica: body.notaTeorica,
        notaInteres: body.notaInteres,
        notaAsistencia: body.notaAsistencia,
        notaPractica: body.notaPractica,
        notaExamen: body.notaExamen,
        notaPromedio: body.notaPromedio,
        ingreso: body.ingreso,
        inscrito: body.inscrito,
        oferta: body.oferta,
        participante: body.participante,
        usuario: req.usuario._id
    });

    inscritoNota.save((err, inscritoNotaGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al registrar nota',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            inscritoNota: inscritoNotaGuardado
        });


    });

});


// ============================================
//   Borrar un inscritoNota por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    InscritoNota.findByIdAndRemove(id, (err, inscritoNotaBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar nota',
                errors: err
            });
        }

        if (!inscritoNotaBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un registro de nota con ese id',
                errors: { message: 'No existe un registro de nota con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            inscritoNota: inscritoNotaBorrado
        });

    });

});


module.exports = app;