var express = require('express');

var app = express();

var Agente = require('../models/agente');
var Acb = require('../models/acb');
var Curso = require('../models/curso');
var Participante = require('../models/participante');
var Nivel1 = require('../models/nivel1');
var Nivel2 = require('../models/nivel2');
var Nivel3 = require('../models/nivel3');
var Nivel4 = require('../models/nivel4');
var Oferta = require('../models/oferta');
var Usuario = require('../models/usuario');

// ==============================
// Busqueda por colección
// ==============================
app.get('/coleccion/:tabla/:busqueda/', (req, res) => {

    var busqueda = req.params.busqueda;
    var tabla = req.params.tabla;
    var regex = new RegExp(busqueda, 'i');

    var promesa;

    switch (tabla) {

        case 'usuarios':
            promesa = buscarUsuarios(busqueda, regex);
            break;

        case 'participantes':
            promesa = buscarParticipantes(busqueda, regex);
            break;

        case 'cursos':
            promesa = buscarCursos(busqueda, regex);
            break;

        case 'agentes':
            promesa = buscarAgentes(busqueda, regex);
            break;

        case 'niveles1':
            promesa = buscarNiveles1(busqueda, regex);
            break;

        case 'niveles2':
            promesa = buscarNiveles2(busqueda, regex);
            break;

        case 'niveles3':
            promesa = buscarNiveles3(busqueda, regex);
            break;

        case 'niveles4':
            promesa = buscarNiveles4(busqueda, regex);
            break;

        case 'ofertas':
            promesa = buscarOfertas(busqueda, regex);
            break;

        case 'cursoOfertas':
            promesa = buscarCursoOfertas(busqueda, regex);
            break;

        case 'acbs':
            promesa = buscarAcbs(busqueda, regex);
            break;

        default:
            return res.status(400).json({
                ok: false,
                mensaje: 'Los tipos de busqueda sólo son: usuarios, participantes, agentes, niveles1, niveles2, niveles3, niveles4, ofertas y cursos',
                error: { message: 'Tipo de tabla/coleccion no válido' }
            });

    }

    promesa.then(data => {

        res.status(200).json({
            ok: true,
            [tabla]: data
        });

    })

});


// ==============================
// Busqueda general
// ==============================
app.get('/todo/:busqueda', (req, res, next) => {

    var busqueda = req.params.busqueda;
    var regex = new RegExp(busqueda, 'i');


    Promise.all([
            buscarCursos(busqueda, regex),
            buscarParticipantes(busqueda, regex),
            buscarUsuarios(busqueda, regex),
            buscarAgentes(busqueda, regex),
            buscarNiveles1(busqueda, regex),
            buscarNiveles2(busqueda, regex),
            buscarNiveles3(busqueda, regex),
            buscarNiveles4(busqueda, regex),
            buscarOfertas(busqueda, regex),
            buscarCursoOfertas(busqueda, regex),
            buscarAcbs(busqueda, regex)
        ])
        .then(respuestas => {

            res.status(200).json({
                ok: true,
                cursos: respuestas[0],
                participantes: respuestas[1],
                usuarios: respuestas[2],
                agentes: respuestas[3],
                niveles1: respuestas[4],
                niveles2: respuestas[5],
                niveles3: respuestas[6],
                niveles4: respuestas[7],
                ofertas: respuestas[8],
                cursoOfertas: respuestas[9],
                acbs: respuestas[10]
            });
        });


});


function buscarCursos(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Curso.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, cursos) => {

                if (err) {
                    reject('Error al cargar cursos', err);
                } else {
                    resolve(cursos);
                }
            });
    });
}

function buscarAgentes(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Agente.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, agentes) => {

                if (err) {
                    reject('Error al cargar agentes', err);
                } else {
                    resolve(agentes);
                }
            });
    });
}

function buscarParticipantes(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Participante.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .populate('curso')
            .exec((err, participantes) => {

                if (err) {
                    reject('Error al cargar participantes', err);
                } else {
                    resolve(participantes);
                }
            });
    });
}

function buscarNiveles1(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Nivel1.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, niveles1) => {

                if (err) {
                    reject('Error al cargar niveles1', err);
                } else {
                    resolve(niveles1);
                }
            });
    });
}

function buscarNiveles2(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Nivel2.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, niveles2) => {

                if (err) {
                    reject('Error al cargar niveles2', err);
                } else {
                    resolve(niveles2);
                }
            });
    });
}

function buscarNiveles3(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Nivel3.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, niveles3) => {

                if (err) {
                    reject('Error al cargar niveles3', err);
                } else {
                    resolve(niveles3);
                }
            });
    });
}

function buscarNiveles4(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Nivel4.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, niveles4) => {

                if (err) {
                    reject('Error al cargar niveles4', err);
                } else {
                    resolve(niveles4);
                }
            });
    });
}

function buscarOfertas(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Oferta.find({ nombre: regex })
            .populate('usuario', 'nombre email img')
            .exec((err, ofertas) => {

                if (err) {
                    reject('Error al cargar ofertas', err);
                } else {
                    resolve(ofertas);
                }
            });
    });
}

function buscarCursoOfertas(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Oferta.find({ descripcion: regex }, 'descripcion')
            .populate('curso', 'nombre')
            .populate('usuario', 'nombre email img')
            .exec((err, cursoOfertas) => {

                if (err) {
                    reject('Error al cargar ofertas', err);
                } else {
                    resolve(cursoOfertas);
                }
            });
    });
}


function buscarAcbs(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Acb.find({ nombreCientifico: regex }, 'nombreCientifico')
            .populate('agente')
            .populate('usuario', 'nombre email img')
            .exec((err, acbs) => {

                if (err) {
                    reject('Error al cargar acbs', err);
                } else {
                    resolve(acbs);
                }
            });
    });
}

function buscarUsuarios(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Usuario.find({}, 'nombre email role img')
            .or([{ 'nombre': regex }, { 'email': regex }])
            .exec((err, usuarios) => {

                if (err) {
                    reject('Erro al cargar usuarios', err);
                } else {
                    resolve(usuarios);
                }


            })


    });
}



module.exports = app;