var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Agente = require('../models/agente');

// ==========================================
// Obtener todos los agentes
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Agente.find({})
        .skip(desde)
        .limit(5)
        .populate('nivel1')
        .populate('nivel2')
        .populate('nivel3')
        .populate('nivel4')
        .populate('usuario', 'nombre email')
        .exec(
            (err, agentes) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando agente',
                        errors: err
                    });
                }

                Agente.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        agentes: agentes,
                        total: conteo
                    });

                })

            });
});

// ==========================================
// Obtener agente
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Agente.findById(id)
        .populate('nivel1')
        .populate('nivel2')
        .populate('nivel3')
        .populate('nivel4')
        .populate('usuario', 'nombre email img')
        .exec((err, agente) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar agente',
                    errors: err
                });
            }

            if (!agente) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El agente con el id ' + id + ' no existe',
                    errors: { message: 'No existe un agente con ese ID' }
                });
            }

            res.status(200).json({
                ok: true,
                agente: agente
            });

        })


});

// ==========================================
// Actualizar Agente
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Agente.findById(id, (err, agente) => {


        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar agente',
                errors: err
            });
        }

        if (!agente) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El agente con el id ' + id + ' no existe',
                errors: { message: 'No existe un agente con ese ID' }
            });
        }


        agente.nombre = body.nombre;
        agente.usuario = req.usuario._id;
        agente.nivel1 = body.nivel1;
        agente.nivel2 = body.nivel2;
        agente.nivel3 = body.nivel3;
        agente.nivel4 = body.nivel4;

        agente.save((err, agenteGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar agente',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                agente: agenteGuardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo agente
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var agente = new Agente({
        nombre: body.nombre,
        usuario: req.usuario._id,
        nivel1: body.nivel1,
        nivel2: body.nivel2,
        nivel3: body.nivel3,
        nivel4: body.nivel4
    });

    agente.save((err, agenteGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear agente',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            agente: agenteGuardado
        });


    });

});


// ============================================
//   Borrar un agente por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Agente.findByIdAndRemove(id, (err, agenteBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar agente',
                errors: err
            });
        }

        if (!agenteBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un agente con ese id',
                errors: { message: 'No existe un agente con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            agente: agenteBorrado
        });

    });

});


module.exports = app;