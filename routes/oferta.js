var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Oferta = require('../models/oferta');

// ==========================================
// Obtener todos los ofertas de un curso
// ==========================================
app.get('/curso/:idCurso', (req, res, next) => {

    var desde = req.query.desde || 0;
    var idCurso = req.params.idCurso;
    desde = Number(desde);

    Oferta.find({ curso: idCurso })
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .populate('curso')
        .exec(
            (err, ofertas) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando oferta',
                        errors: err
                    });
                }

                Oferta.count({ curso: idCurso }, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        ofertas: ofertas,
                        total: conteo
                    });

                })

            });
});


// ===========================================
// Obtener todos los ofertas y todos los curso
// ===========================================
app.get('/cursoOfertas', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Oferta.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .populate('curso')
        .exec(
            (err, cursoOfertas) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando ofertas',
                        errors: err
                    });
                }

                Oferta.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        cursoOfertas: cursoOfertas,
                        total: conteo
                    });

                })

            });
});

// ==========================================
// Obtener oferta
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Oferta.findById(id)
        .populate('usuario', 'nombre email img')
        .populate('curso')
        .exec((err, oferta) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar oferta',
                    errors: err
                });
            }

            if (!oferta) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El oferta con el id ' + id + ' no existe',
                    errors: { message: 'No existe un oferta con ese ID' }
                });
            }

            res.status(200).json({
                ok: true,
                oferta: oferta
            });

        })


});

// ==========================================
// Actualizar Oferta
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Oferta.findById(id, (err, oferta) => {


        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar oferta',
                errors: err
            });
        }

        if (!oferta) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El oferta con el id ' + id + ' no existe',
                errors: { message: 'No existe un oferta con ese ID' }
            });
        }
        oferta.nombre = body.nombre;
        oferta.descripcion = body.descripcion;
        oferta.fechaInicio = body.fechaInicio;
        oferta.fechaFin = body.fechaFin;
        oferta.horaInicio = body.horaInicio;
        oferta.horaFin = body.horaFin;
        oferta.horas = body.horas;
        oferta.precio = body.precio;
        oferta.vacantes = body.vacantes;
        oferta.inscritos = body.inscritos;
        oferta.docente = body.docente;
        oferta.temas = body.temas;
        oferta.usuario = req.usuario._id;
        oferta.curso = body.curso;

        oferta.save((err, ofertaGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar oferta',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                oferta: ofertaGuardado
            });

        });

    });

});

//
// ==========================================
// Crear un nueva oferta
// ==========================================
app.post('/:idCurso', mdAutenticacion.verificaToken, (req, res) => {
    var idCurso = req.params.idCurso;
    var body = req.body;

    var oferta = new Oferta({
        nombre: body.nombre,
        descripcion: body.descripcion,
        fechaInicio: body.fechaInicio,
        fechaFin: body.fechaFin,
        horaInicio: body.horaInicio,
        horaFin: body.horaFin,
        horas: body.horas,
        precio: body.precio,
        vacantes: body.vacantes,
        inscritos: body.inscritos,
        docente: body.docente,
        temas: body.temas,
        usuario: req.usuario._id,
        curso: idCurso
    });

    oferta.save((err, ofertaGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear oferta',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            oferta: ofertaGuardado
        });


    });

});


// ============================================
//   Borrar un oferta por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Oferta.findByIdAndRemove(id, (err, ofertaBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar oferta',
                errors: err
            });
        }

        if (!ofertaBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un oferta con ese id',
                errors: { message: 'No existe un oferta con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            oferta: ofertaBorrado
        });

    });

});


module.exports = app;