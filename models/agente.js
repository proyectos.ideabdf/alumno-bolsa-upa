var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var agenteSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    nivel1: { type: Schema.Types.ObjectId, ref: 'Nivel1' },
    nivel2: { type: Schema.Types.ObjectId, ref: 'Nivel2' },
    nivel3: { type: Schema.Types.ObjectId, ref: 'Nivel3' },
    nivel4: { type: Schema.Types.ObjectId, ref: 'Nivel4' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'agentes' });

module.exports = mongoose.model('Agente', agenteSchema);