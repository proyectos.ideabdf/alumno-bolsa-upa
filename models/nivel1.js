var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var nivel1Schema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'Niveles1' });



module.exports = mongoose.model('Nivel1', nivel1Schema);