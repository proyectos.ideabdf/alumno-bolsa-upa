var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var cursoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    descripcion: { type: String, required: [true, 'La descripción es necesario'] },
    tipo: { type: String, required: false },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'cursos' });



module.exports = mongoose.model('Curso', cursoSchema);